# Actividad6 BC (BookControl) 
Descripción de Sistema BC (Book Control)
Actualmente en la institución educativa Liceo de Computación CSS, no cuenta con sistema informático que le ayude a llevar el control de inventario de libros, el único control que tiene actualmente es un archivo de Excel donde se describe a detalle el control de dichos libros es por eso que su información no está sistematizada de forma más óptima, es por eso que el sistema BC permitirá ayudar a la institución con dicho
Objetivo
Optimizara el tiempo de procesamiento de información de datos (libros) y un mejor control de los bienes de la institución
Funciones Básicas del Sistema
1. Logarse  al sistema (usuario y contraseña)
2. Realizar búsqueda de libros 
3. Mantenimiento al sistema de libros (ingresar, modificar e inactivar libros)
4. Crear usuarios 
5. Registrar, Actualizar, Eliminar autores de los libros 
6. Registrar, Actualizar, Eliminar editorial 
7. Importar inventarios hacia archivos externos (Excely pdf)


